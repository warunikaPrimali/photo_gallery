import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-status-card',
  templateUrl: './status-card.component.html',
  styleUrls: ['./status-card.component.scss']
})
export class StatusCardComponent implements OnInit {

  //provided as Inputs to this component refer summary component
  @Input() cardTitle: string;
  @Input() statusValue: string;
  @Input() bgColour: string;

  constructor() { }

  ngOnInit() {
  }

}
