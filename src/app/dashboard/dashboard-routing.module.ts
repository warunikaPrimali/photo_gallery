//contains Dashboard module routing configuartions

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlbumListComponent } from './album-list/album-list.component';
import { AlbumViewComponent } from './album-view/album-view.component';
import { DashboardComponent } from './dashboard.component';
import { SummaryComponent } from './summary/summary.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      { path: '', redirectTo: 'wellcome', pathMatch: 'full' },
      { path: 'wellcome', component: SummaryComponent },
      { path: 'albums/:id', component: AlbumListComponent },
      { path: ':userid/photos/:id', component: AlbumViewComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
