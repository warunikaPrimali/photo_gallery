//Class to represent User Model

export class User {
    id: Number
    name: string;
    username: string;
    email: string;
    address: object;
    phone: string;
    website: string;
    company: object;
    albumCount: Number

    constructor(obj) {
        this.id = (obj || {}).id;
        this.name = (obj || {}).name;
        this.username = (obj || {}).username;
        this.email = (obj || {}).email;
        this.address = (obj || {}).address;
        this.phone = (obj || {}).phone;
        this.website = (obj || {}).website;
        this.company = (obj || {}).company;
        this.albumCount = 0

    }
}