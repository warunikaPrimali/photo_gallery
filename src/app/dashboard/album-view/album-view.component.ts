import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Constants } from 'src/app/shared/constants';
import { LoaderService } from 'src/app/shared/loader.service';
import { Album } from '../Album';
import { DashboardService } from '../dashboard.service';
import { Photo } from '../Photo';
import { User } from '../User';

@Component({
  selector: 'app-album-view',
  templateUrl: './album-view.component.html',
  styleUrls: ['./album-view.component.scss']
})
export class AlbumViewComponent implements OnInit {

  id: Number;
  userId: Number;
  photos: Photo[];
  user: User;
  album: Album;

  constructor(private route: ActivatedRoute, private dashboardService: DashboardService, private router: Router, private loader: LoaderService) { }

  ngOnInit() {
    //get albumId and UserId from params
    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.userId = params['userid'];
    });
    if (this.id && this.userId) {
      this.getPhotos()
      this.getAlbumData()
      this.getUserData()
    } else {
      this.router.navigate([Constants.navigateDashBoard]);
    }
  }

   /**
   * Method to get Photo data
   * Check if existing data
   * else get the particular Photos from API
   * @example getAlbumData()
   */
  getPhotos() {
    let photoData = this.dashboardService.getPhotos();
    if (photoData) {
      this.photos = photoData.filter((photo) => photo.albumId == this.id);
    } else {
      this.loader.updateCounter(1);
      this.dashboardService.getAllPhotosByAlbumId(this.id).subscribe((photos) => {
        this.loader.updateCounter(-1);
        this.photos = photos;
      })
    }
  }

   /**
   * Method to get AlbumData
   * Check if existing data
   * else get the particular Album Data from API
   * @example getAlbumData()
   */
  getAlbumData() {
    let albumData = this.dashboardService.getAlbums();
    if (albumData) {
      this.album = albumData.filter((album) => album.id == this.id)[0];
    } else {
      this.dashboardService.getAlbumByAlbumId(this.id).subscribe((album) => {
        this.album = album;
      })
    }
  }

  /**
   * Method to get UserData
   * Check if existing data
   * else get the particular user data from API
   * @example getUserData()
   */
  getUserData() {
    let userData = this.dashboardService.getUsers();
    if (userData) {
      this.user = userData.filter((user) => user.id == this.userId)[0];
    } else {
      this.dashboardService.getUserById(this.userId).subscribe((user) => {
        this.user = user;
      })
    }
  }

}
