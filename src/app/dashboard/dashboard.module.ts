//represent dashboard module Configurations

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxPaginationModule } from 'ngx-pagination';
import { ChartsModule } from 'ng2-charts';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { StatusCardComponent } from './status-card/status-card.component';
import { AlbumViewComponent } from './album-view/album-view.component';
import { AlbumListComponent } from './album-list/album-list.component';
import { AlbumDetailsComponent } from './album-details/album-details.component';
import { SummaryComponent } from './summary/summary.component';

@NgModule({
  declarations: [DashboardComponent, StatusCardComponent, AlbumViewComponent, AlbumListComponent, AlbumDetailsComponent, SummaryComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    SharedModule,
    NgxPaginationModule,
    ChartsModule
  ]
})
export class DashboardModule { }
