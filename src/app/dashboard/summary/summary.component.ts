import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChartType } from 'chart.js';
import { Label, SingleDataSet } from 'ng2-charts';
import { Constants } from 'src/app/shared/constants';
import { LoaderService } from 'src/app/shared/loader.service';
import { DashboardService } from '../dashboard.service';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {
  //Chart configurations

  public doughnutChartLabels: Label[] = ['Less than 5 Albums', 'Between 6-10 Albums', 'More than 10 Albums'];
  public doughnutChartData: SingleDataSet = [];
  public doughnutChartType: ChartType = Constants.doughnutType;
  public options = {
    cutoutPercentage: 80,
    legend: {
      display: false
    },
  }
  public colors = [
    {
      backgroundColor: ['#36b9cd', '#1cc98a', '#4e73de'],
    }
  ];

  //end

  public userCount: Number;
  public albumCount: Number;
  public photoCount: Number;
  public users = [];
  p: number = 1;
  countdown: number;

  constructor(private dashboardService: DashboardService, private router: Router, private loader: LoaderService) { }

  ngOnInit() {
    this.getAllUsersAndAlbums();
    this.getAllPhotos();
  }

  /** 
   * This method is to get all users and albums
   * First it will get user data
   * Then it will get albums data and update album count accordingly
   * Update chartData
   * Update user count and album count
   * @example getAllUsersAndAlbums()
  */
  getAllUsersAndAlbums() {
    this.loader.updateCounter(1);
    this.dashboardService.getAllUsers().subscribe((data) => {
      if (data) {
        this.userCount = data.length;
        let userData = data
        this.dashboardService.getAllAlbums().subscribe((albums) => {
          this.loader.updateCounter(-1);
          if (albums) {
            this.albumCount = albums.length
            this.dashboardService.setAlbums(albums);
            this.users = userData.map((item, index) => {
              item.albumCount = albums.filter((album, index) => album.userId === item.id).length
              return item;
            })
            this.dashboardService.setUsers(this.users);
          }
          if (this.users && this.users.length !== 0) {
            this.doughnutChartData = [this.users.filter((item) => item.albumCount <= 5).length, this.users.filter((item) => item.albumCount <= 10 && item.albumCount > 5).length, this.users.filter((item) => item.albumCount > 10).length]
          }
        })
      }

    })
  }

  /**
   * This method is to get photos from API
   * save data in the service and update photos count
   */
  getAllPhotos() {
    this.dashboardService.getAlPhotos().subscribe((data) => {
      if (data) {
        this.photoCount = data.length;
        this.dashboardService.setPhotos(data);
      }
    })
  }

  //navigate to album list view with userId
  goToAlbums(id) {
    this.router.navigate([Constants.navigateAlbums + id])
  }


}
