import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Constants } from '../shared/constants';
import { Album } from './Album';
import { Photo } from './Photo';
import { User } from './User';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  baseUrl = Constants.apiUrl
  private albums: Album[]
  private photos: Photo[]
  private users: User[]

  constructor(private http: HttpClient) { }

  /**Method to call to get previously saved data
   * @return {Album[]} - returns an array contains album data
   * @example - getAlbums()
   */
  getAlbums() {
    return this.albums;
  }

  /**Method to store album data in the service
   * @param {Album[]} albums - an array parm contains album data
   * @example - setAlbums(albums)
 */
  setAlbums(albums: Album[]) {
    this.albums = albums;
  }

  /**Method to get all album data from the Api
   * @return {Album[]} - returns an array contains album data
   * @example - getAllAlbums()
 */
  getAllAlbums(): Observable<Album[]> {
    const url = Constants.apiUrl + Constants.albums;
    return this.http.get<Album[]>(url);
  }



  /**Method to get all albums owns by particular  user from the Api
   * @param {Number} id - an number parm for userId
   * @return {Album[]} - returns an array contains album data
   * @example - getAllAlbumByUserId(id)
 */
  getAllAlbumByUserId(id): Observable<Album[]> {
    const url = Constants.apiUrl + Constants.albumsByUserId + id;
    return this.http.get<Album[]>(url);
  }


  /**Method to get  particular album from the Api
   * @param {Number} id - an number parm for albumId
   * @return {Album[]} - returns an array contains album data
   * @example - getAlbumByAlbumId(id)
 */
  getAlbumByAlbumId(id): Observable<Album> {
    const url = Constants.apiUrl + Constants.album + id;
    return this.http.get<Album>(url);
  }


  /**Method to call to get previously saved data
 * @return {Photo[]} - returns an array contains photo data
 * @example - getPhotos()
 */
  getPhotos() {
    return this.photos;
  }

  /**Method to get all photos by particular album Id from API
  * @param {Number} id - an number parm for albumId
  * @return {Photo[]} - returns an array contains photo data
  * @example - getAllPhotosByAlbumId(id)
  */
  getAllPhotosByAlbumId(id): Observable<Photo[]> {
    const url = Constants.apiUrl + Constants.photosByAlbumId + id;
    return this.http.get<Photo[]>(url);
  }


  /**Method to store photo data in the service
* @param {Photo[]} photos - an array containing photo data
* @example - setPhotos(photos: Photo[])
*/
  setPhotos(photos: Photo[]) {
    this.photos = photos;
  }

  /**Method to get all photo data from api
* @return {Photo[]} - returns an array contains photo data
* @example - getAlPhotos()
*/
  getAlPhotos(): Observable<Photo[]> {
    const url = Constants.apiUrl + Constants.photos;
    return this.http.get<Photo[]>(url);
  }


  /**Method to get stored data
* @return {user[]} - returns an array contains user data
* @example - getUsers()
*/
  getUsers() {
    return this.users;
  }


  /**Method to store data in the service
* @param {user[]} users - an array parm containg userData
* @example - setUsers(users)
*/
  setUsers(users: User[]) {
    this.users = users;
  }

  /**Method to get all users from API
* @param {user[]} - an array parm containg userData
* @example - getAllUsers()
*/
  getAllUsers(): Observable<User[]> {
    const url = Constants.apiUrl + Constants.users;
    return this.http.get<User[]>(url);
  }

  /**Method to get particular user data from API
* @param {Number} id- number param for UserId
* @example - getUserById()
*/
  getUserById(id): Observable<User> {
    const url = Constants.apiUrl + Constants.userByUserId + id;
    return this.http.get<User>(url);
  }
}
