import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Constants } from 'src/app/shared/constants';
import { Album } from '../Album';
@Component({
  selector: 'app-album-details',
  templateUrl: './album-details.component.html',
  styleUrls: ['./album-details.component.scss']
})
export class AlbumDetailsComponent implements OnInit {

  //provided as Inputs this component refer album-list-component.html
  @Input() album: Album;
  @Input() userId

  constructor(private router: Router) { }

  ngOnInit() {
  }

  //navigate to view the photos of selected album
  viewPhotos(id) {
    this.router.navigate([Constants.navigateDashBoard + this.userId + Constants.navigatePhotos + id])
  }

}
