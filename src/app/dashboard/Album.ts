//Class to represent Album Model

export class Album {
    userId: Number
    id: Number
    title: string;
    to: string;
    thumbnailUrl: string;

    constructor(obj) {
        this.id = (obj || {}).id;
        this.userId = (obj || {}).userId;
        this.title = (obj || {}).title;
        this.to = (obj || {}).to;
        this.thumbnailUrl = (obj || {}).thumbnailUrl;
    }
}