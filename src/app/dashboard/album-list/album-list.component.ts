import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { Router } from '@angular/router';

import { User } from '../User';
import { Album } from '../Album';
import { DashboardService } from '../dashboard.service';
import { Constants } from 'src/app/shared/constants';
import { LoaderService } from 'src/app/shared/loader.service';
@Component({
  selector: 'app-album-list',
  templateUrl: './album-list.component.html',
  styleUrls: ['./album-list.component.scss']
})
export class AlbumListComponent implements OnInit {

  id: Number;
  albums: Album[]
  user: User

  constructor(private route: ActivatedRoute, private dashboardService: DashboardService, private router: Router, private loader: LoaderService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });
    if (this.id) {
      this.getAlbumData();
      this.getUserData();
    } else {
      this.router.navigate([Constants.navigateDashBoard]);
    }
  }

  /**
   * Method to get Albumdata
   * Check if existing data
   * if data set thumbnail image for each albums by using photos
   * else get the particular albumData from API 
   * and set thumbnail image for each albums by using photos
   * @example - getAlbumData()
   */

  getAlbumData() {
    let AlbumData = this.dashboardService.getAlbums();
    let photoData = this.dashboardService.getPhotos();
    if (AlbumData && photoData) {
      let albumsOwnByUser = AlbumData.filter((item)=>item.userId == this.id)
      this.albums = (albumsOwnByUser || []).map((item, index) => {
        item.thumbnailUrl = (photoData || []).filter((photo) => item.id === photo.albumId)[0].thumbnailUrl;
        return item;
      })
    } else {
      this.loader.updateCounter(1);
      this.dashboardService.getAllAlbumByUserId(this.id).subscribe((albums) => {
        this.loader.updateCounter(-1);
        let albumSet = []
        albums.map((item, index) => {
          this.dashboardService.getAllPhotosByAlbumId(item.id).subscribe((photos) => {
            item.thumbnailUrl = (photos || [])[0].thumbnailUrl;
            albumSet.push(item)
          })
        })
        this.albums = albumSet;
      })
    }
  }

  /**
   * Method to get UserData
   * Check if existing data
   * else get the particular user data from API
   * @example getUserData()
   */
  getUserData() {
    let userData = this.dashboardService.getUsers()
    if (userData) {
      this.user = userData.filter((user) => user.id == this.id)[0]
    } else {
      this.dashboardService.getUserById(this.id).subscribe((user) => {
        this.user = user;
      })
    }
  }

}
