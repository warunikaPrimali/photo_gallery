import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../shared/loader.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public isLoaderLoded = true;

  constructor(private loader: LoaderService) { }

  /* In this onInit method it will looks for loading activity changes
   and will mark the componnets to show the spinner
  */
  ngOnInit() {
    this.loader.requestCounter.subscribe((data) => {
      this.isLoaderLoded = (data === 0)
    }
    );
  }

}
