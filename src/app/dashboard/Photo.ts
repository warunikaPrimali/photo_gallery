//Class to represent User Photo

export class Photo{
    albumId: Number
    id:Number
    url:string;
    thumbnailUrl:string;
    constructor(obj) {
        this.id = (obj || {}).id;
        this.albumId = (obj || {}).albumId;
        this.url = (obj || {}).url;
        this.thumbnailUrl = (obj || {}).thumbnailUrl;
    }
}