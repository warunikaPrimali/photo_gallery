import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { LoaderService } from './loader.service';
import { InvalidPageComponent } from './invalid-page/invalid-page.component';

@NgModule({
  declarations: [InvalidPageComponent],
  imports: [
    CommonModule,
    SharedRoutingModule
  ],
  exports: [],
  providers: [
    LoaderService
  ]
})
export class SharedModule { }
