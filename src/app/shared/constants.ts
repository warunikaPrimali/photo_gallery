//contains constants used By application

export class Constants {
    public static readonly navigateDashBoard = '/dashboard/';
    public static readonly navigateAlbums = '/dashboard/albums/';
    public static readonly navigatePhotos = '/photos/';

    public static readonly doughnutType = 'doughnut';

    public static readonly apiUrl = 'https://jsonplaceholder.typicode.com';

    public static readonly albums ='/albums';
    public static readonly albumsByUserId ='/albums?userId=';
    public static readonly album = '/albums/';

    public static readonly photosByAlbumId = '/photos?albumId=';
    public static readonly photos='/photos';

    public static readonly users = '/users';
    public static readonly userByUserId = '/users/'


}
