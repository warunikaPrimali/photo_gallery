import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  public photos: BehaviorSubject<boolean> = new BehaviorSubject<any>([]);

  constructor() { }

  setData(data){
    this.photos = data;
  }

  getData(){
    return this.photos;
  }
}
