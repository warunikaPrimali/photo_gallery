//this service is to manage loder for api calls
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class LoaderService {

  _count: number;
  private messageSource = new BehaviorSubject<number>(0);
  requestCounter = this.messageSource.asObservable();


  constructor() {
    this._count = 0;
  }

  public updateCounter(n) {
    this._count = this._count + n;
    this.messageSource.next(this._count);
  }
}
