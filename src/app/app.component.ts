
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'gallery';

  ngOnInit() {
    console.log('test CICD');
  }

  //this method is to manage toggle drawer navabar when resolution chnage to mobile
  toggleNav() {
    document.getElementById("wrapper").classList.toggle("toggled");
  }
}
