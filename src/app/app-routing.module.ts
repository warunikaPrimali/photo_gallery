//this is to manage application main routing configuarations
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvalidPageComponent } from './shared/invalid-page/invalid-page.component';

export let routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
  { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule) },
  { path: '**', component: InvalidPageComponent}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [
    RouterModule]
})
export class AppRoutingModule { }
